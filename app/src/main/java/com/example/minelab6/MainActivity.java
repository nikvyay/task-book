package com.example.minelab6;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Task> states = new ArrayList<Task>();

    TaskStorage taskStorage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        TaskStorage.Init(this);
        taskStorage = TaskStorage.getInstance();
        refreshTasks();

        FloatingActionButton fab = findViewById(R.id.buttonAdd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTaskActivity();
            }
        });
    }
    private void addTaskActivity()
    {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra("action", "add");
        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear:
                clearTask();
                return true;

            case R.id.exit:
                exit();
                return true;
            default:
                return true;
        }
    }
    private ArrayList<Task> getTasksList ()
    {
        try {
            return taskStorage.getTasks();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new ArrayList<Task>();

    }
    public void refreshTasks(){
        states = getTasksList();

        RecyclerView recyclerView = findViewById(R.id.list);
        TaskAdapter adapter = new TaskAdapter(this, states);
        recyclerView.setAdapter(adapter);
    }

    private void clearTask()
    {
        taskStorage.deleteTasks();
        refreshTasks();
    }
    private void exit ()
    {
        this.finishAffinity();
    }

    public void editTask (Task task)
    {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra("action", "edit");
        intent.putExtra("id", task.getId());
        intent.putExtra("name", task.getName());
        intent.putExtra("description", task.getDescription());
        String date = (new SimpleDateFormat(TaskStorage.pattern, TaskStorage.locale))
                .format(task.getDateAndTime().getTime());
        intent.putExtra("dateAndTime", date);
        intent.putExtra("priority", Task.stringPriority(task.getPriority()));
        startActivity(intent);
    }
}