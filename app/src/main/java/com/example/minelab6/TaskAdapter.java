package com.example.minelab6;

import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder>
{
    private final LayoutInflater inflater;
    private final List<Task> tasks;
    private MainActivity context;

    TaskAdapter(MainActivity context, List<Task> tasks) {
        this.tasks = tasks;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }
    @Override
    public TaskAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskAdapter.ViewHolder holder, int position) {
        Task task = tasks.get(position);
        holder.nameView.setText(task.getName());

        String date = DateUtils.formatDateTime(context,
                task.getDateAndTime().getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR);
        holder.dateView.setText(date);

        String time = DateUtils.formatDateTime(context,
                task.getDateAndTime().getTimeInMillis(),
                DateUtils.FORMAT_SHOW_TIME);
        holder.timeView.setText(time);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TaskStorage.getInstance().deleteTask(task);
                context.refreshTasks();
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.editTask(task);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView nameView, dateView, timeView;
        final ImageView delete, edit;
        ViewHolder(View view){
            super(view);
            nameView = view.findViewById(R.id.name);
            dateView = view.findViewById(R.id.date);
            timeView = view.findViewById(R.id.time);
            delete = view.findViewById(R.id.imageViewDelete);
            edit = view.findViewById(R.id.imageViewEdit);
        }
    }
}
